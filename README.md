# Appian Community Automated Test Framework
1. [Required Software](#required-software)
2. [Initial Set Up](#initial-set-up)
3. [Selenium Grid](#selenium-grid)
4. [Executing Tests](#executing-tests)
5. [Watching Test Runs](#watching-test-runs)
6. [Learning Resources](#learning-resources)

## Required Software
* [Docker](https://docs.docker.com/install/)
* [Python 3.7.6](https://www.python.org/downloads/release/python-376/)
* [VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/)

## Initial Set Up
### Clone the Repository
Via HTTPS: `https://gitlab.com/clinedim/appian-interview-deliverable.git`

Via SSH: `git@gitlab.com:clinedim/appian-interview-deliverable.git`

### Install the Project's Dependencies
Change into the project folder:

`cd /path/to/appian-interview-deliverable`

Begin by setting up a [Virtual Environment](https://docs.python.org/3.7/tutorial/venv.html) in the project root:

`python3 -m venv venv`

Next, activate your virtual environment:

`source venv/bin/activate`

Install all the dependencies from the [requirements.txt](requirements.txt) file:

`pip install -r requirements.txt`

## Selenium Grid
We run all of our tests through an instance of [Selenium Grid](https://www.selenium.dev/documentation/en/grid/).

### Setting Up Selenium Grid
Our [docker-compose.yml](https://gitlab.com/clinedim/appian-interview-deliverable/-/blob/master/docker-compose.yml) file
contains a basic configuration for an instance of Selenium Grid that contains 1 Chrome browser and 1 Firefox browser.

Execute the following command to spin up Selenium Grid Docker containers:

`docker-compose up -d`

After Docker finishes running, you can verify the the setup was successful by navigating to:

[http://localhost:4444/grid/console](http://localhost:4444/grid/console)

### Tearing Down Selenium Grid
Execute the following command to tear down the Selenium Grid Docker containers:

`docker-compose down`

## Executing Tests
The tests in this framework can be executed via the command line. The sections below contain instructions on how to
execute the tests.

### Required Environment Variables
This framework expects the `ENV_PASSWORD` and `ENV_USERNAME` environment variables to contain the password and username
of a valid Appian account. In order to use the framework, set these variables:

```
export ENV_PASSWORD=password_goes_here
export ENV_USERNAME=the_username_goes_here@email.com
```

### Optional Environment Variables
If you plan to execute tests against [BrowserStack](https://www.browserstack.com/automate), you must set the
`ENV_BS_PASSWORD` and `ENV_BS_USERNAME` environment variables to contain the password and username for your BrowserStack
account. You can find your credentials in [Account Settings](https://www.browserstack.com/accounts/settings) on
BrowserStack. Set these variables like this:

```
export ENV_BS_PASSWORD=password_goes_here
export ENV_BS_USERNAME=username_goes_here
```

You can also provide a name for the test suite that will appear within the BrowserStack interface by setting the 
`ENV_BS_TEST_SUITE_NAME` environment variable:

```
export ENV_BS_TEST_SUITE_NAME="Test Suite Name Goes Here"
```

### Test Commands
The tests can be executed against Chrome with one of the following commands:

* __Local Chrome:__ `pytest --browser chrome`
* __BrowserStack Chrome:__ `pytest --browser chrome-bs`

Likewise, the tests can be executed against Firefox with one of the following commands:

* __Local Firefox:__  `pytest --browser firefox`
* __BrowserStack Firefox:__ `pytest --browser firfox-bs`

__Note:__ You must have set your BrowserStack credentials in order to use BrowserStack, or you will receive an
authentication error.

## Watching Test Runs
It can be useful to watch a test suite as it executes, particularly when writing a new test suite or diagnosing a test
suite failure. This is the primary use of [VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/).

The sections below show the required settings for viewing tests running through Chrome and Firefox. If prompted, the
password for the virtual machine is `secret`.

### Chrome VNC Viewer Settings
![Chrome VNC Viewer Settings](vnc_viewer_chrome.png)

### Firefox VNC Viewer Settings
![Firefox VNC Viewer Settings](vnc_viewer_firefox.png)

## Learning Resources
### Python Resources
Here are some of my favorite Python resources:

* [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)
* [Dive into Python 3](https://diveintopython3.problemsolving.io/)
* [How to Think Like a Computer Scientist](https://runestone.academy/runestone/books/published/thinkcspy/index.html)
* [Official pytest Documentation](https://docs.pytest.org/en/latest/)
* [Official Python Tutorial](https://docs.python.org/3.7/tutorial/)

### Selenium Resources
Here are some of my favorite Selenium resources:

* [Distributed Testing with Selenium Grid and Docker](https://testdriven.io/blog/distributed-testing-with-selenium-grid/)
* [Selenium Python Documentation](https://selenium-python.readthedocs.io/installation.html)
* [Selenium Webdriver with Python 3 Udemy Course](https://www.udemy.com/course/selenium-webdriver-with-python3/)
* [The Selenium Guidebook](https://seleniumguidebook.com/)
