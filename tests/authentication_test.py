import logging
import pytest

from utilities.credentials import load_user_credentials_from_environment_variables


@pytest.mark.authentication
class TestAuthentication:
    @pytest.fixture(autouse=True)
    def set_up_test_case(self, login_page):
        """
        This function runs before each of the test functions that follow. It simply
        navigates to the login page URL so that the user has a fresh state at the
        beginning of each test case.
        """
        login_page.navigate()

    def test_login_page_renders_correctly(self, login_page):
        """
        Test Case: The user sees the correct elements on the page when navigating
        to the login page.

        Preconditions: The user is on the login page.

        Steps:
        1. Verify that the login page has the header "SIGN IN USING YOUR EMAIL ADDRESS."
        2. Verify that the username field has the placeholder "EMAIL ADDRESS."
        3. Verify that the password field has the placeholder "PASSWORD."
        4. Verify that the "Sign In" button is visible.
        """
        logging.info("Verify that the page has the correct header.")
        assert login_page.page_header == "SIGN IN USING YOUR EMAIL ADDRESS"

        logging.info("Verify that the username field has the correct placeholder.")
        assert login_page.username_field_placeholder == "EMAIL ADDRESS"

        logging.info("Verify that the password field has the correct placeholder.")
        assert login_page.password_field_placeholder == "PASSWORD"

        logging.info("Verify that the 'Sign In' button is visible.")
        assert login_page.sign_in_button_is_visible

    @pytest.mark.parametrize(
        "username, password",
        [
            ["", ""],
            ["this_is_a_fake_email@gmail.com", "this_is_an_invalid_password"],
            ["email_with_no_password@gmail.com", ""],
            ["", "password_with_no_email"],
            ["marc.clinedinst@fiscalnote.com", ""],
        ],
    )
    def test_user_sees_expected_error_message_after_invalid_login_attempt(
        self, login_page, password, username
    ):
        """
        Test Case: The user receives the expected error message after an invalid
        login attempt.

        Preconditions: The user is on the login page. The user has not entered
        anything in the username or password field.

        Steps:
        1. Enter an invalid username and password combination.
        2. Verify that the expected error message appears.
        """
        login_page.log_in(username, password)

        logging.info("Verify that the expected error message appears.")
        assert login_page.error_messages == ["Authentication Failed"]

    def test_user_can_log_in_and_log_out(
        self, home_page, login_page, navigation_menu, sign_out_page
    ):
        """
        Test Case: The user can log in with a valid username and password combination
        and then log out.

        Preconditions: The user is on the login page. The user has not entered anything
        in the username or password field.
        """
        credentials = load_user_credentials_from_environment_variables()
        login_page.log_in(credentials["username"], credentials["password"])

        logging.info(
            "Verify that the 'Recent Activity' header is visible on the home page."
        )
        assert home_page.page_header == "Recent Activity"

        navigation_menu.click_profile_icon()
        navigation_menu.click_sign_out_link()

        logging.info("Verify that the user is on the 'Sign out' page.")
        assert sign_out_page.page_header == "Sign out"

        logging.info("Verify that the user is on the correct URL.")
        assert sign_out_page.current_url == "https://community.appian.com/logout"
