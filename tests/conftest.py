import os
import pytest

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from pages import HomePage, LoginPage, NavigationMenu, SignOutPage
from tests import config

BS_BUILD = "Appian Community"
BS_TEST_SUITE_NAME = os.environ.get(
    "ENV_BS_TEST_SUITE_NAME", "No Name Provided - Set ENV_BS_TEST_SUITE_NAME"
)
BS_PASSWORD = os.environ.get("ENV_BS_PASSWORD", "")
BS_USERNAME = os.environ.get("ENV_BS_USERNAME", "")
BS_URL = f"http://{BS_USERNAME}:{BS_PASSWORD}@hub-cloud.browserstack.com/wd/hub"
LOCAL_SELENIUM_GRID_URL = "http://localhost:4444/wd/hub"

ACCEPTED_BROWSERS = {
    "chrome": {
        "command_executor": LOCAL_SELENIUM_GRID_URL,
        "desired_capabilities": DesiredCapabilities.CHROME,
    },
    "chrome-bs": {
        "command_executor": BS_URL,
        "desired_capabilities": {
            "acceptSslCerts": True,
            "browser": "Chrome",
            "browser_version": "80",
            "build": BS_BUILD,
            "name": BS_TEST_SUITE_NAME,
            "os": "Windows",
            "os_version": "10",
            "resolution": "1920x1080",
        },
    },
    "firefox": {
        "command_executor": LOCAL_SELENIUM_GRID_URL,
        "desired_capabilities": DesiredCapabilities.FIREFOX,
    },
    "firefox-bs": {
        "command_executor": BS_URL,
        "desired_capabilities": {
            "browser": "Firefox",
            "browser_version": "81",
            "build": BS_BUILD,
            "name": BS_TEST_SUITE_NAME,
            "os": "Windows",
            "os_version": "10",
            "resolution": "1920x1080",
        },
    },
}


def pytest_addoption(parser):
    parser.addoption(
        "--browser", action="store", default="chrome", help="browser used for testing"
    )


def get_driver(desired_browser):
    browser = ACCEPTED_BROWSERS.get(desired_browser)

    if not browser:
        browsers = ", ".join(ACCEPTED_BROWSERS.keys())
        raise Exception(
            f"Unrecognized browser: '{desired_browser}'; accepted options: {browsers}"
        )

    return webdriver.Remote(
        command_executor=browser["command_executor"],
        desired_capabilities=browser["desired_capabilities"],
    )


@pytest.fixture(scope="session")
def driver(request):
    config.browser = request.config.getoption("--browser")

    _driver = get_driver(config.browser)
    _driver.maximize_window()

    def _quit():
        _driver.quit()

    request.addfinalizer(_quit)

    return _driver


@pytest.fixture
def home_page(driver):
    return HomePage(driver)


@pytest.fixture
def login_page(driver):
    return LoginPage(driver)


@pytest.fixture
def navigation_menu(driver):
    return NavigationMenu(driver)


@pytest.fixture
def sign_out_page(driver):
    return SignOutPage(driver)
