from selenium.common.exceptions import StaleElementReferenceException, TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.expected_conditions import (
    element_to_be_clickable,
    invisibility_of_element,
    presence_of_element_located,
    presence_of_all_elements_located,
    visibility_of_all_elements_located,
    visibility_of_element_located,
)
from selenium.webdriver.support.wait import WebDriverWait

BASE_TIMEOUT = 15


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    @property
    def current_url(self):
        """
        :return: This property returns the current URL in the browser's address bar.
        """
        return self.driver.current_url

    def _click(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to click on an element. An exception will occur if the
        element is not clickable.

        :param locator: The locator for the element to click.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be clickable.
        :return: None
        """
        self._find_clickable_element(locator, timeout=timeout).click()

    def _find_clickable_element(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to find a clickable element within the user interface.
        If a clickable element is not found with the specified locator, the None value
        will be returned.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be clickable.
        :return: A WebElement matching the locator or None.
        """
        try:
            wait = WebDriverWait(self.driver, timeout)

            return wait.until(
                element_to_be_clickable((locator["by"], locator["value"]))
            )
        except TimeoutException:
            return None

    def _find_present_element(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to find an element present within the user interface;
        the element does not need to be visible. If an element is not found with the
        specified locator, the None value will be returned.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the
        element to be present.
        :return: A WebElement matching the locator or None.
        """
        try:
            wait = WebDriverWait(self.driver, timeout)

            return wait.until(
                presence_of_element_located((locator["by"], locator["value"]))
            )
        except TimeoutException:
            return None

    def _find_present_elements(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to find a list of elements present within the user
        interface; the elements need not be visible. If no element is found with
        the specified locator, the function returns an empty list.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the
        element(s) to be present.
        :return: A list of WebElement matching the locator, or an empty list.
        """
        try:
            wait = WebDriverWait(self.driver, timeout)

            return wait.until(
                presence_of_all_elements_located((locator["by"], locator["value"]))
            )
        except TimeoutException:
            return []

    def _find_visible_element(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to find a visible element within the user interface.
        If a visible element is not found with the specified locator, the None value
        will be returned.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be visible.
        :return: A WebElement matching the locator or None.
        """
        try:
            wait = WebDriverWait(self.driver, timeout)

            return wait.until(
                visibility_of_element_located((locator["by"], locator["value"]))
            )
        except TimeoutException:
            return None

    def _find_visible_elements(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to find a list of elements visible within the user
        interface. If no visible element is
        found with the specified locator, the function returns an empty list.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the
        element to be visible.
        :return: A list of WebElement matching the locator, or an empty list.
        """
        try:
            wait = WebDriverWait(self.driver, timeout)

            return wait.until(
                visibility_of_all_elements_located((locator["by"], locator["value"]))
            )
        except TimeoutException:
            return []

    def _get_attribute_from_element(
        self, locator, attribute_name, timeout=BASE_TIMEOUT
    ):
        """
        This method can be used to get the value of an attribute for an element present
        within the user interface.

        :param locator: The locator for the element to find.
        :param attribute_name: The name of the attribute for which to fetch the value.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be visible.
        :return: The value of the attribute or None.
        """
        element = self._find_present_element(locator, timeout)

        return element.get_attribute(attribute_name)

    def _get_text_from_element(
        self, locator, split_and_join=True, strip=True, timeout=BASE_TIMEOUT
    ):
        """
        This method is used to get the text from a particular element. An exception will
        raise if the element is not found.

        :param locator: The locator for the element to find.
        :param split_and_join: Whether to split and join the string; this can be used to
        remove extra internal whitespace within the string.
        :param strip: Whether to remove whitespace at the beginning and the end of the
        string.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be visible.
        :return: The text from the found element.
        """
        element_text = self._find_visible_element(locator, timeout=timeout).text

        if split_and_join:
            element_text = " ".join(element_text.split())

        if strip:
            element_text = element_text.strip()

        return element_text

    def _get_text_from_elements(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to get text from multiple elements that match the same
        locator.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be visible.
        :return: A list of strings for any matched elements; an empty list if no
        elements match the specified locator.
        """
        elements = self._find_present_elements(locator, timeout)

        return [" ".join(element.text.split()).strip() for element in elements]

    def _is_not_visible(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to determine if an element is visible. For AJAX
        applications, it will also wait for a specified amount of time for the element
        to disappear.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be
        visible.
        :return:
        """
        try:
            wait = WebDriverWait(self.driver, timeout)
            wait.until(invisibility_of_element((locator["by"], locator["value"])))

            return True
        except TimeoutException:
            return False

    def _is_visible(self, locator, timeout=BASE_TIMEOUT):
        """
        This method can be used to determine if an element is visible. For AJAX
        applications, it will also wait for a specified amount of time for the element
        to appear.

        :param locator: The locator for the element to find.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be visible.
        :return:
        """
        return self._find_visible_element(locator, timeout=timeout) is not None

    def _type(
        self,
        locator,
        input_text,
        clear_input=False,
        terminate_with_return_key=False,
        timeout=BASE_TIMEOUT,
    ):
        """
        This method can be used to type text into an input field. The method may also
        optionally clear the input and/or press the Return key after typing text.

        :param locator: The locator for the element to find.
        :param input_text: The text to type into the field.
        :param clear_input: Whether to clear the input before entering text.
        :param terminate_with_return_key: Whether to press the Return key after
        entering text.
        :param timeout: The maximum amount of time (in seconds) to wait for the element
        to be visible.
        :return: None
        """
        input_element = self._find_clickable_element(locator, timeout=timeout)

        if clear_input:
            input_element.clear()

        input_element.send_keys(input_text)

        if terminate_with_return_key:
            input_element.send_keys(Keys.RETURN)

    def _wait_for_text_in_element_to_equal(
        self, locator, expected_text, retries=3, timeout=BASE_TIMEOUT
    ):
        """
        This method can be used to wait for text to appear in an element. This is
        useful during page transitions where elements on different pages share the
        same locator.

        :param locator: The locator for the element to find.
        :param expected_text: The text that is expected to appear in an element.
        :param timeout: The maximum amount of time (in seconds) to wait for the
        element to be visible.
        :return: The expected text or None.
        """
        try:
            seconds_waited = 0

            while seconds_waited < timeout:
                element = self._find_visible_element(locator, timeout=1)

                if element and element.text.strip() == expected_text.strip():
                    return element.text

                seconds_waited = seconds_waited + 1
        except StaleElementReferenceException:
            if retries > 0:
                return self._wait_for_text_in_element_to_equal(
                    locator, expected_text, retries=retries - 1
                )
