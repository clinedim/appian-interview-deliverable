import logging

from . import locators
from pages.base_page import BasePage


class LoginPage(BasePage):
    @property
    def error_messages(self):
        error_messages = self._get_text_from_elements(locators.ERROR_MESSAGE)
        self._is_not_visible(locators.ERROR_MESSAGE)

        return error_messages

    @property
    def page_header(self):
        return self._find_visible_element(locators.PAGE_HEADER).text

    @property
    def password_field_placeholder(self):
        return self._get_attribute_from_element(locators.PASSWORD_INPUT, "placeholder")

    @property
    def sign_in_button_is_visible(self):
        return self._is_visible(locators.SIGN_IN_BUTTON)

    @property
    def username_field_placeholder(self):
        return self._get_attribute_from_element(locators.USERNAME_INPUT, "placeholder")

    def click_sign_in_button(self):
        logging.info("Click the 'Sign In' button.")
        self._click(locators.SIGN_IN_BUTTON)

    def enter_password(self, password):
        logging.info("Enter password in the 'Password' field.")
        self._type(locators.PASSWORD_INPUT, password, clear_input=True)

    def enter_username(self, username):
        logging.info(f"Enter '{username}' in the 'Username' field.")
        self._type(locators.USERNAME_INPUT, username, clear_input=True)

    def log_in(self, username, password):
        self.enter_username(username)
        self.enter_password(password)
        self.click_sign_in_button()

    def navigate(self):
        login_page_url = "https://login.appian.com/sso/XUI/#login"
        logging.info(f"Navigate to {login_page_url}.")
        self.driver.get(login_page_url)
