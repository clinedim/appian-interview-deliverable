from selenium.webdriver.common.by import By

ERROR_MESSAGE = {"by": By.CSS_SELECTOR, "value": "#messages .message"}

PAGE_HEADER = {"by": By.CSS_SELECTOR, "value": ".page-header h1"}

PASSWORD_INPUT = {"by": By.ID, "value": "idToken2"}

SIGN_IN_BUTTON = {"by": By.ID, "value": "loginButton_0"}

USERNAME_INPUT = {"by": By.ID, "value": "idToken1"}
