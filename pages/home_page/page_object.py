from . import locators
from pages.base_page import BasePage


class HomePage(BasePage):
    @property
    def page_header(self):
        return self._get_text_from_element(locators.RECENT_ACTIVITY_HEADER)
