from selenium.webdriver.common.by import By

RECENT_ACTIVITY_HEADER = {
    "by": By.CSS_SELECTOR,
    "value": ".activity-story-stream .content-fragment-header",
}
