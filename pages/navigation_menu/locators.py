from selenium.webdriver.common.by import By

PROFILE_ICON = {"by": By.ID, "value": "header-509_user"}

SIGN_OUT_LINK = {"by": By.CSS_SELECTOR, "value": ".navigation-list-item.logout a"}
