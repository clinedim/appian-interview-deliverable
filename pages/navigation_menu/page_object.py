import logging

from . import locators
from pages.base_page import BasePage


class NavigationMenu(BasePage):
    def click_profile_icon(self):
        logging.info("Click the profile icon on the top right corner of the page.")
        self._click(locators.PROFILE_ICON)

    def click_sign_out_link(self):
        logging.info("Click the 'Sign out' link in the user dropdown menu.")
        self._click(locators.SIGN_OUT_LINK)
