from . import locators
from pages.base_page import BasePage


class SignOutPage(BasePage):
    @property
    def page_header(self):
        return self._wait_for_text_in_element_to_equal(
            locators.SIGN_OUT_HEADER, "Sign out"
        )
