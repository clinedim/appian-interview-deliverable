from selenium.webdriver.common.by import By

SIGN_OUT_HEADER = {"by": By.CSS_SELECTOR, "value": ".content-fragment-content h1"}

SIGN_OUT_MESSAGE = {"by": By.CSS_SELECTOR, "value": ".content-fragment-content p"}
